// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall_Destro.h"
#include "SnakeBase.h"

// Sets default values
AWall_Destro::AWall_Destro()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWall_Destro::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall_Destro::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall_Destro::Interact(AActor* Interator, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interator);

	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"
#include "SnakeBase.h"
#include "Food.h"
#include "TimerManager.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(15);
}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);

	if (bIsHead)
	{
		if (IsValid(Snake))
		{
			Snake->MovementSpeed = 0.1;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			this->Destroy();

			FTimerHandle TimerHandle;
			GetWorldTimerManager().SetTimer(TimerHandle, Snake, &ASnakeBase::ReturnNormalSpeed, 7.f, false);
		}
	}
}


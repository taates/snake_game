// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "BonusBase.h"
#include "TimerManager.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandle2;
	GetWorldTimerManager().SetTimer(TimerHandle2, this, &AFood::BonusSpawn, 30.f, true);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElements();
			//this->Destroy();

			if (RandFood)
			{
				FVector NewFoodLocation((rand() % 400), (rand() % 900), 0);
				this->SetActorLocation(NewFoodLocation);
				RandFood = false;
			}
			else
			{
				FVector NewFoodLocation((rand() % 400 * (-1)), (rand() % 900 * (-1)), 0);
				this->SetActorLocation(NewFoodLocation);
				RandFood = true;
			}
				//FVector NewFoodLocation((rand() % 500 * (-1)), (rand() % 500), 0);
				//FVector NewFoodLocation((rand() % 500), (rand() % 500 * (-1)), 0);
		}
	}
}

void AFood::BonusSpawn()
{
	if (RandFood)
	{
		FVector NewBonusLocation((rand() % 400 * (-1)), (rand() % 900), 0);
		FTransform NewTransform(NewBonusLocation);
		ABonusBase* Bonus = GetWorld()->SpawnActor<ABonusBase>(BonusElementClass, NewTransform);
	}
	else
	{
		FVector NewBonusLocation((rand() % 400), (rand() % 900 * (-1)), 0);
		FTransform NewTransform(NewBonusLocation);
		ABonusBase* Bonus = GetWorld()->SpawnActor<ABonusBase>(BonusElementClass, NewTransform);
	}
}
